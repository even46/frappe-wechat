<?php
// +----------------------------------------------------------------------
// | Wechat
// +----------------------------------------------------------------------
// | 日期 2020-06-16
// +----------------------------------------------------------------------
// | 开发者 Even <even@1000duo.cn>
// +----------------------------------------------------------------------
// | 版权所有 2020~2021 苏州千朵网络科技有限公司 [ https://www.1000duo.cn ]
// +----------------------------------------------------------------------

namespace frappe\wechat\open;

use think\facade\Log;
use frappe\wechat\lib\Tools;
use frappe\wechat\lib\DataArray;
use frappe\wechat\exceptions\InvalidArgumentException;
use frappe\wechat\exceptions\InvalidResponseException;
use frappe\wechat\exceptions\LocalCacheException;
use frappe\wechat\mp\Receive;

/**
 * 开放平台接口
 * Class Open
 * @package wechat
 */
class Service
{
    /**
     * 当前配置对象
     * @var DataArray
     */
    protected $config;

    /**
     * Service constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {
        if (empty($options['component_token'])) {
            throw new InvalidArgumentException("Missing Config -- [component_token]");
        }
        if (empty($options['component_appid'])) {
            throw new InvalidArgumentException("Missing Config -- [component_appid]");
        }
        if (empty($options['component_appsecret'])) {
            throw new InvalidArgumentException("Missing Config -- [component_appsecret]");
        }
        if (empty($options['component_encodingaeskey'])) {
            throw new InvalidArgumentException("Missing Config -- [component_encodingaeskey]");
        }
        if (!empty($options['cache_path'])) {
            Tools::$cache_path = $options['cache_path'];
        }
        $this->config = new DataArray($options);
    }

    /**
     * 接收公众平台推送的 Ticket
     * @return bool|array
     * @throws InvalidResponseException
     * @throws LocalCacheException
     */
    public function getComonentTicket()
    {
        $receive = new Receive([
            'token' => $this->config->get('component_token'),
            'appid' => $this->config->get('component_appid'),
            'appsecret' => $this->config->get('component_appsecret'),
            'encodingaeskey' => $this->config->get('component_encodingaeskey'),
            'cache_path' => $this->config->get('cache_path'),
        ]);
        $data = $receive->getReceive();
        Log::info(json_encode($data));
        if (!empty($data['ComponentVerifyTicket'])) {
            Tools::setCache('component_verify_ticket', $data['ComponentVerifyTicket']);
        }
        return $data;
    }

    /**
     * 获取或刷新服务 AccessToken
     * @return bool|string
     * @throws InvalidResponseException
     * @throws LocalCacheException
     */
    public function getComponentAccessToken()
    {
        # TODO: Tools::getCache($cache);
        $cache = 'wechat_component_access_token';
        if (($component_access_token = Tools::getCache($cache))) {
            return $component_access_token;
        }
        $data = [
            'component_appid' => $this->config->get('component_appid'),
            'component_appsecret' => $this->config->get('component_appsecret'),
            'component_verify_ticket' => Tools::getCache('component_verify_ticket'),
            # TODO: Tools::getCache('component_verify_ticket'),
        ];
        $url = 'https://api.weixin.qq.com/cgi-bin/component/api_component_token';
        $result = $this->httpPostForJson($url, $data);
        if (empty($result['component_access_token'])) {
            throw new InvalidResponseException($result['errmsg'], $result['errcode'], $data);
        }
        Tools::setCache($cache, $result['component_access_token'], 3600);
        # TODO: Tools::setCache($cache, $result['component_access_token'], 7000);
        return $result['component_access_token'];
    }

    /**
     * 获取授权方的帐号基本信息
     * @param string $authorizer_appid 授权公众号或小程序的appid
     * @return array
     * @throws InvalidResponseException
     * @throws LocalCacheException
     */
    public function getAuthorizerInfo($authorizer_appid)
    {
        $component_access_token = $this->getComponentAccessToken();
        $url = "https://api.weixin.qq.com/cgi-bin/component/api_get_authorizer_info?component_access_token={$component_access_token}";
        $data = [
            'authorizer_appid' => $authorizer_appid,
            'component_appid' => $this->config->get('component_appid'),
        ];
        $result = $this->httpPostForJson($url, $data);
        if (empty($result['authorizer_info'])) {
            throw new InvalidResponseException($result['errmsg'], $result['errcode'], $data);
        }
        return $result['authorizer_info'];
    }

    /**
     * 设置授权方的选项信息
     * @param string $authorizer_appid 授权公众号或小程序的appid
     * @param string $option_name 选项名称
     * @param string $option_value 设置的选项值
     * @return array
     * @throws InvalidResponseException
     * @throws LocalCacheException
     */
    public function setAuthorizerOption($authorizer_appid, $option_name, $option_value)
    {
        $component_access_token = $this->getComponentAccessToken();
        $url = "https://api.weixin.qq.com/cgi-bin/component/api_set_authorizer_option?component_access_token={$component_access_token}";
        $result = $this->httpPostForJson($url, [
            'option_name' => $option_name,
            'option_value' => $option_value,
            'authorizer_appid' => $authorizer_appid,
            'component_appid' => $this->config->get('component_appid'),
        ]);
        return $result;
    }

    /**
     * 获取预授权码 pre_auth_code
     * @return string
     * @throws InvalidResponseException
     * @throws LocalCacheException
     */
    public function getPreauthCode()
    {
        $component_access_token = $this->getComponentAccessToken();
        $url = "https://api.weixin.qq.com/cgi-bin/component/api_create_preauthcode?component_access_token={$component_access_token}";
        $result = $this->httpPostForJson($url, ['component_appid' => $this->config->get('component_appid')]);
        if (empty($result['pre_auth_code'])) {
            throw new InvalidResponseException('GetPreauthCode Faild.', '0', $result);
        }
        return $result['pre_auth_code'];
    }

    /**
     * 获取授权回跳地址
     * @param string $redirect_uri 回调URI
     * @param integer $auth_type 要授权的帐号类型: 3小程序
     * @return bool
     * @throws InvalidResponseException
     * @throws LocalCacheException
     */
    public function getAuthRedirect($redirect_uri, $auth_type = 3)
    {
        $redirect_uri = urlencode($redirect_uri);
        $pre_auth_code = $this->getPreauthCode();
        $component_appid = $this->config->get('component_appid');
        return "https://mp.weixin.qq.com/cgi-bin/componentloginpage?component_appid={$component_appid}&pre_auth_code={$pre_auth_code}&redirect_uri={$redirect_uri}&auth_type={$auth_type}";
    }

    /**
     * 使用授权码换取公众号或小程序的接口调用凭据和授权信息
     * @param null $auth_code 授权码
     * @return bool|array
     * @throws InvalidResponseException
     * @throws LocalCacheException
     */
    public function getQueryAuthorizerInfo($auth_code = null)
    {
        if (is_null($auth_code) && isset($_GET['auth_code'])) {
            $auth_code = $_GET['auth_code'];
        }
        if (empty($auth_code)) {
            return false;
        }
        $component_access_token = $this->getComponentAccessToken();
        $url = "https://api.weixin.qq.com/cgi-bin/component/api_query_auth?component_access_token={$component_access_token}";
        $data = [
            'component_appid' => $this->config->get('component_appid'),
            'authorization_code' => $auth_code,
        ];
        $result = $this->httpPostForJson($url, $data);
        if (empty($result['authorization_info'])) {
            throw new InvalidResponseException($result['errmsg'], $result['errcode'], $data);
        }
        $authorizer_appid = $result['authorization_info']['authorizer_appid'];
        $authorizer_access_token = $result['authorization_info']['authorizer_access_token'];
        // 缓存授权公众号访问 ACCESS_TOKEN
        Tools::setCache("{$authorizer_appid}_access_token", $authorizer_access_token, 7000);
        return $result['authorization_info'];
    }

    /**
     * 获取（刷新）授权公众号的令牌
     * @param string $authorizer_appid 授权公众号或小程序的appid
     * @param string $authorizer_refresh_token 授权方的刷新令牌
     * @return array
     * @throws InvalidResponseException
     * @throws LocalCacheException
     */
    public function refreshAccessToken($authorizer_appid, $authorizer_refresh_token)
    {
        $component_access_token = $this->getComponentAccessToken();
        $url = "https://api.weixin.qq.com/cgi-bin/component/api_authorizer_token?component_access_token={$component_access_token}";
        $data = [
            'authorizer_appid' => $authorizer_appid,
            'authorizer_refresh_token' => $authorizer_refresh_token,
            'component_appid' => $this->config->get('component_appid'),
        ];
        $result = $this->httpPostForJson($url, $data);
        if (empty($result['authorizer_access_token'])) {
            throw new InvalidResponseException($result['errmsg'], $result['errcode'], $data);
        }
        // 缓存授权公众号访问 ACCESS_TOKEN
        Tools::setCache("{$authorizer_appid}_access_token", $result['authorizer_access_token'], 7000);
        return $result;
    }

    /**
     * oauth 授权跳转接口
     * @param string $authorizer_appid 授权公众号或小程序的appid
     * @param string $redirect_uri 回调地址
     * @param string $scope snsapi_userinfo|snsapi_base
     * @return string
     */
    public function getOauthRedirect($authorizer_appid, $redirect_uri, $scope = 'snsapi_userinfo')
    {
        $redirect_url = urlencode($redirect_uri);
        $component_appid = $this->config->get('component_appid');
        return "https://open.weixin.qq.com/connect/oauth2/authorize?appid={$authorizer_appid}&redirect_uri={$redirect_url}&response_type=code&scope={$scope}&state={$authorizer_appid}&component_appid={$component_appid}#wechat_redirect";
    }

    /**
     * 通过code获取AccessToken
     * @param string $authorizer_appid 授权公众号或小程序的appid
     * @return bool|array
     * @throws InvalidResponseException
     * @throws LocalCacheException
     */
    public function getOauthAccessToken($authorizer_appid)
    {
        if (empty($_GET['code'])) {
            return false;
        }
        $component_appid = $this->config->get('component_appid');
        $component_access_token = $this->getComponentAccessToken();
        $url = "https://api.weixin.qq.com/sns/oauth2/component/access_token?appid={$authorizer_appid}&code={$_GET['code']}&grant_type=authorization_code&component_appid={$component_appid}&component_access_token={$component_access_token}";
        $result = $this->httpGetForJson($url);
        return $result !== false ? $result : false;
    }

    /**
     * fastRegisterWeapp 快速创建小程序
     * @param $component_access_token
     * @param $name
     * @param $code
     * @param $code_type
     * @param $legal_persona_wechat
     * @param $legal_persona_name
     * @param $component_phone
     * @return mixed
     * @author Even <even@1000duo.cn>
     * @date 2020/11/10 12:12:58
     */
    public function fastRegisterWeApp($name, $code, $code_type, $legal_persona_wechat, $legal_persona_name, $component_phone)
    {
        $component_access_token = $this->getComponentAccessToken();
        $url = "https://api.weixin.qq.com/cgi-bin/component/fastregisterweapp?action=create&component_access_token={$component_access_token}";
        $data = [
            'name' => $name,
            'code' => $code,
            'code_type' => $code_type,
            'legal_persona_wechat' => $legal_persona_wechat,
            'legal_persona_name' => $legal_persona_name,
            'component_phone' => $component_phone,
        ];
        $result = $this->httpPostForJson($url, $data);
        if (!isset($result['errcode']) || $result['errcode'] !== 0) {
            throw new InvalidResponseException($result['errmsg'], $result['errcode'], $data);
        }
        return $result;
    }

    /**
     * 创建指定授权公众号接口实例
     * @param string $type 需要加载的接口实例名称
     * @param string $authorizer_appid 授权公众号的appid
     * @return \frappe\wechat\mini\Total|\frappe\wechat\mini\Template|\frappe\wechat\mini\Soter|\frappe\wechat\mini\Security|\frappe\wechat\mini\Qrcode|\frappe\wechat\mini\Poi|\frappe\wechat\mini\Plugs|\frappe\wechat\mini\Ocr|\frappe\wechat\mini\Message|\frappe\wechat\mini\Logistics|\frappe\wechat\mini\Image|\frappe\wechat\mini\Delivery|\frappe\wechat\mini\Ocrypt|\frappe\wechat\mp\Card|\frappe\wechat\mp\Custom|\frappe\wechat\mp\Media|\frappe\wechat\mp\Menu|\frappe\wechat\mp\Oauth|\frappe\wechat\mp\Pay|\frappe\wechat\mp\Product|\frappe\wechat\mp\Qrcode|\frappe\wechat\mp\Receive|\frappe\wechat\mp\Scan|\frappe\wechat\mp\Script|\frappe\wechat\mp\Shake|\frappe\wechat\mp\Tags|\frappe\wechat\mp\Template|\frappe\wechat\mp\User|\frappe\wechat\mp\Wifi
     */
    public function instance($type, $authorizer_appid, $s = 'mp')
    {
        $className = 'frappe\wechat\\' . $s . '\\' . ucfirst(strtolower($type));
        return new $className($this->getConfig($authorizer_appid));
    }

    /**
     * 获取授权公众号配置参数
     * @param string $authorizer_appid 授权公众号的appid
     * @return array
     */
    public function getConfig($authorizer_appid)
    {
        $config = $this->config->get();
        $config['appid'] = $authorizer_appid;
        $config['token'] = $this->config->get('component_token');
        $config['appsecret'] = $this->config->get('component_appsecret');
        $config['encodingaeskey'] = $this->config->get('component_encodingaeskey');
        # 回调
        return $config;
    }

    /**
     * 以POST获取接口数据并转为数组
     * @param string $url 接口地址
     * @param array $data 请求数据
     * @param bool $buildToJson
     * @return array
     */
    protected function httpPostForJson($url, array $data, $buildToJson = true)
    {
        return json_decode(Tools::post($url, $buildToJson ? Tools::arr2json($data) : $data), true);
    }

    /**
     * 以GET获取接口数据并转为数组
     * @param string $url 接口地址
     * @return array
     */
    protected function httpGetForJson($url)
    {
        return json_decode(Tools::get($url), true);
    }
}