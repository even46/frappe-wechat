<?php

// +----------------------------------------------------------------------
// | Wechat
// +----------------------------------------------------------------------
// | 日期 2020-06-14
// +----------------------------------------------------------------------
// | 开发者 Even <even@1000duo.cn>
// +----------------------------------------------------------------------
// | 版权所有 2020~2021 苏州千朵网络科技有限公司 [ https://www.1000duo.cn ]
// +----------------------------------------------------------------------

namespace frappe\wechat\mp;

use frappe\wechat\lib\BasicPushEvent;

/**
 * 公众号推送管理
 * Class Receive
 * @package frappe\wechat\mp
 */
class Receive extends BasicPushEvent
{

    /**
     * 转发多客服消息
     * @param string $account
     * @return $this
     */
    public function transferCustomerService($account = '')
    {
        $this->message = [
            'CreateTime'   => time(),
            'ToUserName'   => $this->getOpenid(),
            'FromUserName' => $this->getToOpenid(),
            'MsgType'      => 'transfer_customer_service',
        ];
        empty($account) || $this->message['TransInfo'] = ['KfAccount' => $account];
        return $this;
    }

    /**
     * 设置文本消息
     * @param string $content 文本内容
     * @return $this
     */
    public function text($content = '')
    {
        $this->message = [
            'MsgType'      => 'text',
            'CreateTime'   => time(),
            'Content'      => $content,
            'ToUserName'   => $this->getOpenid(),
            'FromUserName' => $this->getToOpenid(),
        ];
        return $this;
    }

    /**
     * 设置回复图文
     * @param array $newsData
     * @return $this
     */
    public function news($newsData = [])
    {
        $this->message = [
            'CreateTime'   => time(),
            'MsgType'      => 'news',
            'Articles'     => $newsData,
            'ToUserName'   => $this->getOpenid(),
            'FromUserName' => $this->getToOpenid(),
            'ArticleCount' => count($newsData),
        ];
        return $this;
    }

    /**
     * 设置图片消息
     * @param string $mediaId 图片媒体ID
     * @return $this
     */
    public function image($mediaId = '')
    {
        $this->message = [
            'MsgType'      => 'image',
            'CreateTime'   => time(),
            'ToUserName'   => $this->getOpenid(),
            'FromUserName' => $this->getToOpenid(),
            'Image'        => ['MediaId' => $mediaId],
        ];
        return $this;
    }

    /**
     * 设置语音回复消息
     * @param string $mediaid 语音媒体ID
     * @return $this
     */
    public function voice($mediaid = '')
    {
        $this->message = [
            'CreateTime'   => time(),
            'MsgType'      => 'voice',
            'ToUserName'   => $this->getOpenid(),
            'FromUserName' => $this->getToOpenid(),
            'Voice'        => ['MediaId' => $mediaid],
        ];
        return $this;
    }

    /**
     * 设置视频回复消息
     * @param string $mediaid 视频媒体ID
     * @param string $title 视频标题
     * @param string $description 视频描述
     * @return $this
     */
    public function video($mediaid = '', $title = '', $description = '')
    {
        $this->message = [
            'CreateTime'   => time(),
            'MsgType'      => 'video',
            'ToUserName'   => $this->getOpenid(),
            'FromUserName' => $this->getToOpenid(),
            'Video'        => [
                'Title'       => $title,
                'MediaId'     => $mediaid,
                'Description' => $description,
            ],
        ];
        return $this;
    }

    /**
     * 设置音乐回复消息
     * @param string $title 音乐标题
     * @param string $desc 音乐描述
     * @param string $musicurl 音乐地址
     * @param string $hgmusicurl 高清音乐地址
     * @param string $thumbmediaid 音乐图片缩略图的媒体id（可选）
     * @return $this
     */
    public function music($title, $desc, $musicurl, $hgmusicurl = '', $thumbmediaid = '')
    {
        $this->message = [
            'CreateTime'   => time(),
            'MsgType'      => 'music',
            'ToUserName'   => $this->getOpenid(),
            'FromUserName' => $this->getToOpenid(),
            'Music'        => [
                'Title'       => $title,
                'Description' => $desc,
                'MusicUrl'    => $musicurl,
                'HQMusicUrl'  => $hgmusicurl,
            ],
        ];
        if ($thumbmediaid) {
            $this->message['Music']['ThumbMediaId'] = $thumbmediaid;
        }
        return $this;
    }

    /**
     * mini_text 设置小程序文本回复消息
     * @param string $content
     * @return array
     * @author Even <even@1000duo.cn>
     * @date 2021/04/20 09:06:38
     */
    public function mini_text($content = '')
    {
        return [
            'touser' => $this->getOpenid(),
            'msgtype' => 'text',
            'text' => [
                'content' => $content
            ]
        ];
    }

    /**
     * mini_image 设置小程序图片回复消息
     * @param string $media_id
     * @return array
     * @author Even <even@1000duo.cn>
     * @date 2021/04/20 09:06:58
     */
    public function mini_image($media_id = '')
    {
        return [
            'touser' => $this->getOpenid(),
            'msgtype' => 'text',
            'image' => [
                'media_id' => $media_id
            ]
        ];
    }

    /**
     * mini_link 设置小程序图文链接回复消息
     * @param string $title
     * @param string $description
     * @param string $url
     * @param string $thumb_url
     * @return array
     * @author Even <even@1000duo.cn>
     * @date 2021/04/20 09:07:07
     */
    public function mini_link($title = '', $description = '', $url = '', $thumb_url = '')
    {
        return [
            'touser' => $this->getOpenid(),
            'msgtype' => 'text',
            'link' => [
                'title' => $title,
                'description' => $description,
                'url' => $url,
                'thumb_url' => $thumb_url
            ]
        ];
    }

    /**
     * mini_programpage 设置小程序卡片回复消息
     * @param string $title
     * @param string $pagepath
     * @param string $thumb_media_id
     * @return array
     * @author Even <even@1000duo.cn>
     * @date 2021/04/20 09:07:22
     */
    public function mini_programpage($title = '', $pagepath = '', $thumb_media_id = '')
    {
        return [
            'touser' => $this->getOpenid(),
            'msgtype' => 'text',
            'miniprogrampage' => [
                'title' => $title,
                'pagepath' => $pagepath,
                'thumb_media_id' => $thumb_media_id,
            ]
        ];
    }
}