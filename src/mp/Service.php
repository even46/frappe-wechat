<?php
// +----------------------------------------------------------------------
// | shop.9jiexia.com
// +----------------------------------------------------------------------
// | 日期 2021-05-10
// +----------------------------------------------------------------------
// | 开发者 Even <even@1000duo.cn>
// +----------------------------------------------------------------------
// | 版权所有 2020~2021 苏州千朵网络科技有限公司 [ https://www.1000duo.cn ]
// +----------------------------------------------------------------------

namespace frappe\wechat\mp;

use frappe\wechat\lib\DataArray;
use frappe\wechat\lib\Tools;
use frappe\wechat\exceptions\InvalidArgumentException;

/**
 * Class Service
 * @package frappe\wechat\mp
 */
class Service
{
    protected $config;

    /**
     * Service constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {
        if (empty($options['appid'])) {
            throw new InvalidArgumentException("Missing Config -- [appid]");
        }
        if (empty($options['appsecret'])) {
            throw new InvalidArgumentException("Missing Config -- [appsecret]");
        }
        if (!empty($options['cache_path'])) {
            Tools::$cache_path = $options['cache_path'];
        }
        $this->config = new DataArray($options);
    }

    /**
     * instance
     * @param $type
     * @return \frappe\wechat\mp\Card|\frappe\wechat\mp\Custom|\frappe\wechat\mp\Media|\frappe\wechat\mp\Menu|\frappe\wechat\mp\Oauth|\frappe\wechat\mp\Pay|\frappe\wechat\mp\Product|\frappe\wechat\mp\Qrcode|\frappe\wechat\mp\Receive|\frappe\wechat\mp\Scan|\frappe\wechat\mp\Script|\frappe\wechat\mp\Shake|\frappe\wechat\mp\Tags|\frappe\wechat\mp\Template|\frappe\wechat\mp\User|\frappe\wechat\mp\Wifi
     * @author Even <even@1000duo.cn>
     * @date 2021/05/10 16:06:36
     */
    public function instance($type)
    {
        $className = 'frappe\wechat\\mp\\' . ucfirst(strtolower($type));
        return new $className($this->config->get());
    }
}