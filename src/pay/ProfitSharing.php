<?php

// +----------------------------------------------------------------------
// | WeChatDeveloper
// +----------------------------------------------------------------------
// | 版权所有 2014~2018 广州楚才信息科技有限公司 [ http://www.cuci.cc ]
// +----------------------------------------------------------------------
// | 官方网站: http://think.ctolog.com
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | github开源项目：https://github.com/zoujingli/WeChatDeveloper
// +----------------------------------------------------------------------

namespace frappe\wechat\pay;

use frappe\wechat\lib\BasicWePay;

/**
 * 微信分账
 * Class Redpack
 * @package frappe\wechat\pay
 */
class ProfitSharing extends BasicWePay
{

    /**
     * 单次分账
     * @param array $options
     * @return array
     * @throws \frappe\wechat\exceptions\InvalidResponseException
     * @throws \frappe\wechat\exceptions\LocalCacheException
     */
    public function single(array $options)
    {
        $url = "https://api.mch.weixin.qq.com/secapi/pay/profitsharing";
        return $this->callPostApi($url, $options, true);
    }

    /**
     * addReceiver 添加分账接收方
     * @param $options
     * @return array
     * @throws \frappe\wechat\exceptions\InvalidResponseException
     * @throws \frappe\wechat\exceptions\LocalCacheException
     * @author Even <even@1000duo.cn>
     * @date 2021/04/13 22:31:07
     */
    public function addReceiver($options)
    {
        $url = "https://api.mch.weixin.qq.com/pay/profitsharingaddreceiver";
        return $this->callPostApi($url, $options);

    }

}