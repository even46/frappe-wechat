<?php

// +----------------------------------------------------------------------
// | Think Wemini
// +----------------------------------------------------------------------
// | 日期 2020-06-14
// +----------------------------------------------------------------------
// | 开发者 Even <even@1000duo.cn>
// +----------------------------------------------------------------------
// | 版权所有 2020~2021 苏州千朵网络科技有限公司 [ https://www.1000duo.cn ]
// +----------------------------------------------------------------------

namespace frappe\wechat\mini;

use frappe\wechat\lib\BasicWeChat;

/**
 * 小程序生物认证
 * Class Soter
 * @package frappe\wechat\mini
 */
class Soter extends BasicWeChat
{
    /**
     * SOTER 生物认证秘钥签名验证
     * @param array $data
     * @return array
     * @throws \frappe\wechat\exceptions\InvalidResponseException
     * @throws \frappe\wechat\exceptions\LocalCacheException
     */
    public function verifySignature($data)
    {
        $url = 'https://api.weixin.qq.com/cgi-bin/soter/verify_signature?access_token=ACCESS_TOKEN';
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return $this->callPostApi($url, $data, true);
    }

}