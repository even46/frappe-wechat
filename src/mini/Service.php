<?php
// +----------------------------------------------------------------------
// | erp.mini.1000duo.cn
// +----------------------------------------------------------------------
// | 日期 2021-08-04
// +----------------------------------------------------------------------
// | 开发者 Even <even@1000duo.cn>
// +----------------------------------------------------------------------
// | 版权所有 2020~2021 苏州千朵网络科技有限公司 [ https://www.1000duo.cn ]
// +----------------------------------------------------------------------

namespace frappe\wechat\mini;


use frappe\wechat\lib\DataArray;
use frappe\wechat\lib\Tools;
use frappe\wechat\exceptions\InvalidArgumentException;

/**
 * Class Service
 * @package frappe\wechat\mini
 */
class Service
{
    protected $config;

    /**
     * Service constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {
        if (empty($options['appid'])) {
            throw new InvalidArgumentException("Missing Config -- [appid]");
        }
        if (empty($options['appsecret'])) {
            throw new InvalidArgumentException("Missing Config -- [appsecret]");
        }
        if (!empty($options['cache_path'])) {
            Tools::$cache_path = $options['cache_path'];
        }
        $this->config = new DataArray($options);
    }

    /**
     * instance
     * @param $type
     * @return \frappe\wechat\mini\Total|\frappe\wechat\mini\Template|\frappe\wechat\mini\Soter|\frappe\wechat\mini\Security|\frappe\wechat\mini\Qrcode|\frappe\wechat\mini\Poi|\frappe\wechat\mini\Plugs|\frappe\wechat\mini\Ocr|\frappe\wechat\mini\Message|\frappe\wechat\mini\Logistics|\frappe\wechat\mini\Image|\frappe\wechat\mini\Delivery|\frappe\wechat\mini\Ocrypt
     * @author Even <even@1000duo.cn>
     * @date 2021/05/10 16:06:36
     */
    public function instance($type)
    {
        $className = 'frappe\wechat\\mini\\' . ucfirst(strtolower($type));
        return new $className($this->config->get());
    }
}