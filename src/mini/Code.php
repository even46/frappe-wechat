<?php
// +----------------------------------------------------------------------
// | makeitreal
// +----------------------------------------------------------------------
// | 日期 2020-09-17
// +----------------------------------------------------------------------
// | 开发者 Even <even@1000duo.cn>
// +----------------------------------------------------------------------
// | 版权所有 2020~2021 苏州千朵网络科技有限公司 [ https://www.1000duo.cn ]
// +----------------------------------------------------------------------

namespace frappe\wechat\mini;


use frappe\wechat\lib\BasicWeChat;
use frappe\wechat\lib\Tools;

/**
 * Class Code
 * @package frappe\wechat\mini
 */
class Code extends BasicWeChat
{
    /**
     * 上传小程序代码
     * @param $template_id
     * @param array $ext_json
     * @param $user_version
     * @param $user_desc
     * @return array
     * @throws \frappe\wechat\exceptions\InvalidResponseException
     * @throws \frappe\wechat\exceptions\LocalCacheException
     * @author Even <even@1000duo.cn>
     * @date 2020/09/17 22:01:26
     */
    public function commit($template_id, array $ext_json, $user_version, $user_desc)
    {
        $url = 'https://api.weixin.qq.com/wxa/commit?access_token=ACCESS_TOKEN';
        $ext_json = Tools::arr2json($ext_json);
        $data = [
            'template_id' => $template_id,
            'ext_json' => $ext_json,
            'user_version' => $user_version,
            'user_desc' => $user_desc
        ];
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return $this->callPostApi($url, $data);
    }

    /**
     * 获取已上传的代码的页面列表
     * @return array
     * @throws \frappe\wechat\exceptions\InvalidResponseException
     * @throws \frappe\wechat\exceptions\LocalCacheException
     * @author Even <even@1000duo.cn>
     * @date 2020/09/17 22:10:23
     */
    public function getPage()
    {
        $url = 'https://api.weixin.qq.com/wxa/get_page?access_token=ACCESS_TOKEN';
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return $this->callGetApi($url);
    }

    /**
     * memberAuth 获取小程序体验者列表
     * @param string $action
     * @return array
     * @throws \frappe\wechat\exceptions\InvalidResponseException
     * @throws \frappe\wechat\exceptions\LocalCacheException
     * @author Even <even@1000duo.cn>
     * @date 2020/10/12 20:05:02
     */
    public function memberAuth($action = 'get_experiencer')
    {
        $url = 'https://api.weixin.qq.com/wxa/memberauth?access_token=ACCESS_TOKEN';
        $data = [
            'action' => $action,
        ];
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return $this->callPostApi($url, $data);
    }

    /**
     * bindTester 绑定微信用户为体验者
     * @param string $wechatid
     * @return array
     * @throws \frappe\wechat\exceptions\InvalidResponseException
     * @throws \frappe\wechat\exceptions\LocalCacheException
     * @author Even <even@1000duo.cn>
     * @date 2020/10/12 20:06:31
     */
    public function bindTester(string $wechatid)
    {
        $url = 'https://api.weixin.qq.com/wxa/bind_tester?access_token=ACCESS_TOKEN';
        $data = [
            'wechatid' => $wechatid,
        ];
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return $this->callPostApi($url, $data);
    }

    /**
     * unbindTester 解除绑定体验者
     * @param string $userstr userstr 和 wechatid 填写其中一个即可
     * @param bool $isWechatid
     * @return array
     * @throws \frappe\wechat\exceptions\InvalidResponseException
     * @throws \frappe\wechat\exceptions\LocalCacheException
     * @author Even <even@1000duo.cn>
     * @date 2020/10/12 20:09:14
     */
    public function unbindTester(string $userstr, bool $isWechatid = false)
    {
        $url = 'https://api.weixin.qq.com/wxa/unbind_tester?access_token=ACCESS_TOKEN';
        $data = [];
        if ($isWechatid) {
            $data['wechatid'] = $userstr;
        } else {
            $data['userstr'] = $userstr;
        }
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return $this->callPostApi($url, $data);
    }

    /**
     * 获取体验版二维码
     * @param $path
     * @return array
     * @throws \frappe\wechat\exceptions\InvalidResponseException
     * @throws \frappe\wechat\exceptions\LocalCacheException
     * @author Even <even@1000duo.cn>
     * @date 2020/09/17 22:12:53
     */
    public function getQrcode($path)
    {
        $url = 'https://api.weixin.qq.com/wxa/get_qrcode?access_token=ACCESS_TOKEN&path=' . urlencode($path);
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return Tools::get($url);
    }


    public function getDomain()
    {
        $url = 'https://api.weixin.qq.com/wxa/modify_domain?access_token=ACCESS_TOKEN';
        $data = [
            'action' => 'get',
        ];
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return $this->callPostApi($url, $data);
    }

    /**
     * modifyDomain 修改小程序接口域名
     * @param array $requestdomain
     * @param array $wsrequestdomain
     * @param array $uploaddomain
     * @param array $downloaddomain
     * @return array
     * @throws \frappe\wechat\exceptions\InvalidResponseException
     * @throws \frappe\wechat\exceptions\LocalCacheException
     * @author Even <even@1000duo.cn>
     * @date 2020/11/13 09:59:44
     */
    public function modifyDomain(array $requestdomain = [], array $wsrequestdomain = [], array $uploaddomain = [], array $downloaddomain = [])
    {
        $url = 'https://api.weixin.qq.com/wxa/modify_domain?access_token=ACCESS_TOKEN';
        $data = [
            'action' => 'set',
            'requestdomain' => $requestdomain,
            'wsrequestdomain' => $wsrequestdomain,
            'uploaddomain' => $uploaddomain,
            'downloaddomain' => $downloaddomain,
        ];
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return $this->callPostApi($url, $data);
    }

    /**
     * getWebViewDomain 获取业务域名
     * @return array
     * @throws \frappe\wechat\exceptions\InvalidResponseException
     * @throws \frappe\wechat\exceptions\LocalCacheException
     * @author Even <even@1000duo.cn>
     * @date 2020/11/13 12:04:15
     */
    public function getWebViewDomain()
    {
        $url = 'https://api.weixin.qq.com/wxa/setwebviewdomain?access_token=ACCESS_TOKEN';
        $data = [
            'action' => 'get',
        ];
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return $this->callPostApi($url, $data);
    }

    /**
     * setWebViewDomain 设置业务域名
     * @param array $webviewdomain
     * @return array
     * @throws \frappe\wechat\exceptions\InvalidResponseException
     * @throws \frappe\wechat\exceptions\LocalCacheException
     * @author Even <even@1000duo.cn>
     * @date 2020/11/13 12:04:00
     */
    public function setWebViewDomain(array $webviewdomain = [])
    {
        $url = 'https://api.weixin.qq.com/wxa/setwebviewdomain?access_token=ACCESS_TOKEN';
        $data = [
            'action' => 'set',
            'webviewdomain' => $webviewdomain,
        ];
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return $this->callPostApi($url, $data);
    }

    /**
     * submitAudit 提交审核
     * @param array $item_list
     * @param array $preview_info
     * @param string $version_desc
     * @param string $feedback_info
     * @param string $feedback_stuff
     * @param string $ugc_declare
     * @return array
     * @throws \frappe\wechat\exceptions\InvalidResponseException
     * @throws \frappe\wechat\exceptions\LocalCacheException
     * @author Even <even@1000duo.cn>
     * @date 2020/10/12 19:55:19
     */
    public function submitAudit(array $item_list = [], array $preview_info = [], $version_desc = '', $feedback_info = '', $feedback_stuff = '', $ugc_declare = ['scene' => [1]])
    {
        $url = 'https://api.weixin.qq.com/wxa/submit_audit?access_token=ACCESS_TOKEN';
        $data = [
            'item_list' => $item_list,
            'preview_info' => $preview_info,
            'version_desc' => $version_desc,
            'feedback_info' => $feedback_info,
            'feedback_stuff' => $feedback_stuff,
            'ugc_declare' => $ugc_declare,
        ];
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return $this->callPostApi($url, $data);
    }

    /**
     * getLatestAuditStatus 获取最后一次提交审核的状态
     * @return array
     * @throws \frappe\wechat\exceptions\InvalidResponseException
     * @throws \frappe\wechat\exceptions\LocalCacheException
     * @author Even <even@1000duo.cn>
     * @date 2020/10/12 19:56:47
     */
    public function getLatestAuditStatus()
    {
        $url = 'https://api.weixin.qq.com/wxa/get_latest_auditstatus?access_token=ACCESS_TOKEN';
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return $this->callGetApi($url);
    }

    /**
     * undoCodeAudit 小程序审核撤回
     * 单个帐号每天审核撤回次数最多不超过 1 次（每天的额度从0点开始生效），一个月不超过 10 次
     * @return array
     * @throws \frappe\wechat\exceptions\InvalidResponseException
     * @throws \frappe\wechat\exceptions\LocalCacheException
     * @author Even <even@1000duo.cn>
     * @date 2020/10/12 19:58:01
     */
    public function undoCodeAudit()
    {
        $url = 'https://api.weixin.qq.com/wxa/undocodeaudit?access_token=ACCESS_TOKEN';
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return $this->callGetApi($url);
    }

    /**
     * release 发布已通过审核的小程序
     * @param array $data post的data为空，不等于不需要传data，否则会报错【errcode: 44002 "errmsg": "empty post data"】
     * @return array
     * @throws \frappe\wechat\exceptions\InvalidResponseException
     * @throws \frappe\wechat\exceptions\LocalCacheException
     * @author Even <even@1000duo.cn>
     * @date 2020/10/12 19:59:39
     */
    public function release(array $data = [])
    {
        $url = 'https://api.weixin.qq.com/wxa/release?access_token=ACCESS_TOKEN';
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return $this->callPostApi($url, $data);
    }

    /**
     * changeVisitStatus 修改小程序线上代码的可见状态
     * @param bool $open true 可见 false不可见
     * @return array
     * @throws \frappe\wechat\exceptions\InvalidResponseException
     * @throws \frappe\wechat\exceptions\LocalCacheException
     * @author Even <even@1000duo.cn>
     * @date 2020/10/12 20:01:50
     */
    public function changeVisitStatus(bool $open)
    {
        $url = 'https://api.weixin.qq.com/wxa/release?access_token=ACCESS_TOKEN';
        $data = [
            'action' => $open ? 'open' : 'close',
        ];
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return $this->callPostApi($url, $data);
    }

}