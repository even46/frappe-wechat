<?php
// +----------------------------------------------------------------------
// | Think Wemini
// +----------------------------------------------------------------------
// | 日期 2020-06-14
// +----------------------------------------------------------------------
// | 开发者 Even <even@1000duo.cn>
// +----------------------------------------------------------------------
// | 版权所有 2020~2021 苏州千朵网络科技有限公司 [ https://www.1000duo.cn ]
// +----------------------------------------------------------------------

namespace frappe\wechat\mini;

use frappe\wechat\lib\BasicWeChat;
use frappe\wechat\exceptions\InvalidResponseException;

/**
 * 小程序内容安全
 * Class Security
 * @package frappe\wechat\mini
 */
class Security extends BasicWeChat
{

    /**
     * 校验一张图片是否含有违法违规内容
     * @param string $media 要检测的图片文件，格式支持PNG、JPEG、JPG、GIF，图片尺寸不超过 750px x 1334px
     * @return array
     * @throws \frappe\wechat\exceptions\InvalidResponseException
     * @throws \frappe\wechat\exceptions\LocalCacheException
     */
    public function imgSecCheck($media)
    {
        $url = 'https://api.weixin.qq.com/wxa/img_sec_check?access_token=ACCESS_TOKEN';
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return $this->callPostApi($url, ['media' => $media], true);
    }

    /**
     * 异步校验图片/音频是否含有违法违规内容
     * @param string $media_url
     * @param string $media_type
     * @return array
     * @throws InvalidResponseException
     * @throws \frappe\wechat\exceptions\LocalCacheException
     */
    public function mediaCheckAsync($media_url, $media_type)
    {
        $url = 'https://api.weixin.qq.com/wxa/media_check_async?access_token=ACCESS_TOKEN';
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return $this->callPostApi($url, ['media_url' => $media_url, 'media_type' => $media_type], true);
    }

    /**
     * 检查一段文本是否含有违法违规内容
     * @param string $content
     * @return array
     * @throws InvalidResponseException
     * @throws \frappe\wechat\exceptions\LocalCacheException
     */
    public function msgSecCheck($content)
    {
        $url = 'https://api.weixin.qq.com/wxa/msg_sec_check?access_token=ACCESS_TOKEN';
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return $this->callPostApi($url, ['content' => $content], true);
    }
}