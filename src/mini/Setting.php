<?php
// +----------------------------------------------------------------------
// | makeitreal
// +----------------------------------------------------------------------
// | 日期 2020-09-17
// +----------------------------------------------------------------------
// | 开发者 Even <even@1000duo.cn>
// +----------------------------------------------------------------------
// | 版权所有 2020~2021 苏州千朵网络科技有限公司 [ https://www.1000duo.cn ]
// +----------------------------------------------------------------------

namespace frappe\wechat\mini;


use frappe\wechat\lib\BasicWeChat;
use frappe\wechat\lib\Tools;

/**
 * Class Code
 * @package frappe\wechat\mini
 */
class Setting extends BasicWeChat
{
    /**
     * modifyDomain
     * @param string $action add添加|delete删除|set覆盖|get获取
     * @param array $requestdomain ["https://www.qq.com", "https://www.qq.com"]
     * @param array $wsrequestdomain ["wss://www.qq.com", "wss://www.qq.com"]
     * @param array $uploaddomain ["https://www.qq.com", "https://www.qq.com"]
     * @param array $downloaddomain ["https://www.qq.com", "https://www.qq.com"]
     * @return array
     * @throws \frappe\wechat\exceptions\InvalidResponseException
     * @throws \frappe\wechat\exceptions\LocalCacheException
     * @author Even <even@1000duo.cn>
     * @date 2020/09/17 22:46:54
     */
    public function modifyDomain($action, array $requestdomain = [], array $wsrequestdomain = [], array $uploaddomain = [], array $downloaddomain = [])
    {
        $url = 'https://api.weixin.qq.com/wxa/modify_domain?access_token=ACCESS_TOKEN';
        $data = [
            'action' => $action,
            'requestdomain' => $requestdomain,
            'wsrequestdomain' => $wsrequestdomain,
            'uploaddomain' => $uploaddomain,
            'downloaddomain' => $downloaddomain,
        ];
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return $this->callPostApi($url, $data);
    }

    /**
     * modifyDomain
     * @param string $action add添加|delete删除|set覆盖|get获取
     * @param string|array $webviewdomain ["https://www.qq.com", "https://www.qq.com"]
     * @return array
     * @throws \frappe\wechat\exceptions\InvalidResponseException
     * @throws \frappe\wechat\exceptions\LocalCacheException
     * @author Even <even@1000duo.cn>
     * @date 2020/09/17 22:46:54
     */
    public function setWebViewDomain($action, $webviewdomain = [])
    {
        $url = 'https://api.weixin.qq.com/wxa/setwebviewdomain?access_token=ACCESS_TOKEN';
        $data = [
            'action' => $action,
            'webviewdomain' => $webviewdomain,
        ];
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return $this->callPostApi($url, $data);
    }

    /**
     * 获取已上传的代码的页面列表
     * @return array
     * @throws \frappe\wechat\exceptions\InvalidResponseException
     * @throws \frappe\wechat\exceptions\LocalCacheException
     * @author Even <even@1000duo.cn>
     * @date 2020/09/17 22:10:23
     */
    public function getPage()
    {
        $url = 'https://api.weixin.qq.com/wxa/get_page?access_token=ACCESS_TOKEN';
        $this->registerApi($url, __FUNCTION__, func_get_args());
        return $this->callGetApi($url);
    }
}